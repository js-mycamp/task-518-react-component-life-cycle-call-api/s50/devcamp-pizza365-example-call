import "bootstrap/dist/css/bootstrap.min.css"
import FectchAPI from "./component/Fetchapi";
import AxiosLibary from "./component/AxiosLibary";
function App() {
  return (
    <div className="contrainer mt-5 text-center">
      <FectchAPI />
      <hr></hr>
      <AxiosLibary />
    </div>
  );
}

export default App;
